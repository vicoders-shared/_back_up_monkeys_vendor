# listing
Create a shortcode which uses WP_Query in Wordpress

## Installation
```bash
composer require monkeyscode/listing
```

## Usage
## Initialize
Initialize listing by creating an instance of class `Listing`.

```php
use MSC\Listing;

$list = new Listing();
```
Use the module as default: `[listing]`

### Custom your listing
```php
use MSC\Listing;

class MyListing extends Listing
{
    public function __construct()
    {
        parent::__construct();
    }

    public function handle($query, $opts)
    {
        // handle data and display it on front-end
    }
}

new MyListing;
```

### How can we pass our own option for listing

```
$customListing = new Listing;

$myCustomAttr = [
	'option_1' => '',
	'option_2' => ''
];
$customListing->setCustomAttrs($myCustomAttr);
```
Now we can use these options: `[listing option_1="value_1" option_2="value_2"]`

### options
Option | Default | Description | Pattern |
------ | ------  | ----------- | ------- |
post_type | post | Specify type of post | _ |
per_page  | 6    | Number of posts per page | _ |
user      | null | User of the posts | _ |
cat       | null | List of categories | _ |
tag       | null | List of tags | _ |
taxonomy  | null | List of taxonomies and its term | tax1(term_id1:term_id2...),tax2(term2_id1,term2_id2...) |
status    | publish | State of posts which matched to condition | publish,draft,pending... |
excludes  | '' | a string of id of posts which you want to exclude | Ex: "1,2,3,4.." |
orderby   | '' | Fields for order | _ |
paged     | no | Enable paging or not | _ |
filter    | no | Enable/Disable filter feature | _ |
layout    | '' | Pass a template to listing | _ |

## Contributors
[Duy Nguyen](https://github.com/duyngha)

