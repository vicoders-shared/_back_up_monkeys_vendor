<?php

namespace MSC\Customizer;

class Customizer
{
    /**
     * @var   WP_Customize_Manager
     * @since 1.0.0
     */
    protected $customize;

    /**
     * @var   array
     * @since 1.0.0
     */
    protected $fields = [];

    /**
     * @var   array
     * @since 1.0.0
     */
    protected $sections = [];

    /**
     * @var   array
     * @since 1.0.0
     */
    protected $section;    

    /**
     * @var   array
     * @since 1.0.2
     */
    protected $data = [];

    /**
     * initialize
     * 
     * @param  WP_Customize_Manager $customize
     * @since  1.0.0
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;   
    }

    /**
     * register customize
     * 
     * @param  Closure $function
     * @since  1.0.2
     * @return void
     */
    public function create()
    {
        add_action('customize_register', [$this, 'init']);
    }

    /**
     * check and render field type
     * 
     * @param  WP_Customize_Manager $customize
     * @since  1.0.2
     * @return void
     */
    public function init($customize)
    {
        $this->customize = $customize;

        foreach ($this->data as $section) {
            $this->section($section['info']);

            foreach ($section['fields'] as $field) {
                $this->{$field['type']}($field);
            }
        }
    }

    /**
     * add section to customizer
     * 
     * @param  array  $section
     * @since  1.0.0
     * @return void
     */
    public function section($section)
    {
        $this->customize->add_section(
            $section['name'],
            [
                'title'    => __($section['label'], 'monkeyscode'),
                'description' => $section['description'],
                'priority' => $section['priority'],
            ]
        );

        $this->sections[] = $section;
        $this->section = $section;

    }

    /**
     * add setting to customizer
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function setting($field)
    {
        $this->customize->add_setting(
            "{$this->section['name']}_{$field['name']}",
            [
                'default'        => $field['default'],
                'capability'     => 'edit_theme_options',
                'type'           => 'option',
            ]
        );
    }

    /**
     * create text control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function text($field)
    {
        $this->setting($field);
     
        $this->customize->add_control(
            'msc_customize_option_' . $field['name'],
            [
                'label'      => __($field['label'], 'monkeyscode'),
                'section'    => $this->section['name'],
                'settings'   => "{$this->section['name']}_{$field['name']}",
            ]
        );

        $this->fields[] = $field;

    }

    /**
     * create textarea control
     * 
     * @param  object $field
     * @since  1.1.0
     * @return void
     */
    public function textarea($field)
    {
        $this->setting($field);
     
        $this->customize->add_control(
            'msc_customize_option_' . $field['name'],
            [
                'label'      => __($field['label'], 'monkeyscode'),
                'section'    => $this->section['name'],
                'settings'   => "{$this->section['name']}_{$field['name']}",
                'type'       => 'textarea'
            ]
        );

        $this->fields[] = $field;

    }

    /**
     * create radio control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function radio($field)
    {
        $this->setting($field);
     
        $this->customize->add_control(
            'msc_customize_option_' . $field['name'],
            [
                'label'      => __($field['label'], 'monkeyscode'),
                'section'    => $this->section['name'],
                'settings'   => "{$this->section['name']}_{$field['name']}",
                'type'       => 'radio',
                'choices'    => $field['choices'],
            ]
        );

        $this->fields[] = $field;

    }

    /**
     * create checkbox control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function checkbox($field)
    {
        $this->setting($field);
     
        $this->customize->add_control(
            "msc_customize_option_{$field['name']}",
            array(
                'settings' => "{$this->section['name']}_{$field['name']}",
                'label'    => __($field['label'], 'monkeyscode'),
                'section'  => $this->section['name'],
                'type'     => 'checkbox',
            )
        );

        $this->fields[] = $field;

    }

    /**
     * create select control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function select($field)
    {
        $this->setting($field);

        $this->customize->add_control(
            "msc_customize_option_{$field['name']}",
            [
                'settings' => "{$this->section['name']}_{$field['name']}",
                'label'   => __($field['label'], 'monkeyscode'),
                'section' => $this->section['name'],
                'type'    => 'select',
                'choices'    => $field['choices'],
            ]
        );

        $this->fields[] = $field;

    }

    /**
     * create upload image control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function upload($field)
    {

        $this->setting($field);

        $this->customize->add_control(
            new \WP_Customize_Upload_Control(
                $this->customize,
                "msc_customize_option_{$field['name']}",
                [
                    'label'    => __($field['label'], 'monkeyscode'),
                    'section'  => $this->section['name'],
                    'settings' => "{$this->section['name']}_{$field['name']}",
                ]
            )
        );

        $this->fields[] = $field;

    }

    /**
     * create color picker control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function color($field)
    {
        $this->setting($field);

        $this->customize->add_control(
            new \WP_Customize_Color_Control(
                $this->customize,
                "msc_customize_option_{$field['name']}", 
                [
                    'label'    => __($field['label'], 'monkeyscode'),
                    'section'  => $this->section['name'],
                    'settings' => "{$this->section['name']}_{$field['name']}",
                ]
            )
        );

        $this->fields[] = $field;

    }

    /**
     * create page control
     * 
     * @param  object $field
     * @since  1.0.0
     * @return void
     */
    public function page($field)
    {
        $this->setting($field);

        $this->customize->add_control(
            "msc_customize_option_{$field['name']}",
            [
                'label'      => __($field['label'], 'monkeyscode'),
                'section'    => $this->section['name'],
                'type'       => 'dropdown-pages',
                'settings'   => "{$this->section['name']}_{$field['name']}",
            ]
        );

        $this->fields[] = $field;

    }

    /**
     * magic method to get property data
     * 
     * @param  string $name
     * @since  1.0.0
     * @return WP_Error|mixed
     */
    public function __get($name)
    {
        if (!isset($this->$name)) {
            return new \WP_Error(
                'msc_customize_error', 
                __($name . ' property does not exist.', 'monkeyscode')
            );
        }

        return $this->$name;
    }
}