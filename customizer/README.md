# Customizer module
Create theme options

## Installation
```
composer require monkeyscode/customizer
```

## Usage
You have to run under `customize_register` action.

```php
$data = [
    [
        'info' => [
            'name' => 'duy_customize_section',
            'label' => 'Duy Section',
            'description' => '',
            'priority' => 0,
        ],
        'fields' => [
            [
                'name' => 'my_text',
                'type' => 'text',
                'default' => 'hole',
                'label' => 'Quang quac'
            ]
        ]
    ],
    [
        'info' => [
            'name' => 'duy_customize_section_2',
            'label' => 'Duy Section 2',
            'description' => '',
            'priority' => 0,
        ],
        'fields' => [
            [
                'name' => 'my_text_2',
                'type' => 'text',
                'default' => 'hole 2',
                'label' => 'Quang quac 2'
            ]
        ]
    ],
];

$customizer = new Customizer($data);
$customizer->create();

```

### List of Fields
* Text Field

```php
[
    'name' => 'my_field',
    'type' => 'text',
    'default' => 'hole',
    'label' => 'Quang quac'
]
```

* Textarea Field

```php
[
    'name' => 'my_field',
    'type' => 'textarea',
    'default' => 'hole',
    'label' => 'Quang quac'
]
```

* Radio Field

```php
[
    'name' => 'my_field',
    'type' => 'radio',
    'default' => 'hole',
    'label' => 'Quang quac',
    'choices' => [
        1 => 1,
        2 => 2
    ]
]
```

* Checkbox Field

```php
[
    'name' => 'my_field',
    'type' => 'checkbox',
    'default' => 'hole',
    'label' => 'Quang quac',
]
```

* Select Field

```php
[
    'name' => 'my_field',
    'type' => 'select',
    'default' => 'hole',
    'label' => 'Quang quac',
    'choices' => [
        1 => 1,
        2 => 2
    ]
]

```

* Upload Field

```php
[
    'name' => 'my_field',
    'type' => 'upload',
    'default' => 'hole',
    'label' => 'Quang quac',
]
```

* Upload Field

```php
[
    'name' => 'my_field',
    'type' => 'color',
    'default' => 'hole',
    'label' => 'Quang quac',
]
```

## Use in template file
```php
get_option('<section_name>_<field_name>');
```

## Contributor
[Duy Nguyen]()
