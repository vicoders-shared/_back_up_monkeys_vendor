# msc-view
Module for template using laravel blade

## Installation
```bash
composer require monkeyscode/view
```

## Usage
```php
require __DIR__ . '/vendor/autoload.php';

use MSC\FacadeView;

echo FacadeView::render('test');
```

## Contributors
[Duy Nguyen]()
